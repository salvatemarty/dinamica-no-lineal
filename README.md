# Dinamica No Lineal

Este proyecto es un resumen de mi participacion como Jefe de Trabajos Practicos de la materia Dinamica No Lineal que se dicta en la Facultad de Ciencias Astronomicas y Geofisicas de La Plata, y es optativa para las carreras de Astronomia y Fisica.

La idea es presentar las practicas con guias y lineamientos sobre como resolverlas, tanto la parte teorica, como la parte numerica. Las guias de resolucion estan hechas en notebooks de python, y se pueden descargar para ser utilizadas y editadas a gusto de cada uno.

Tambien se encuentran subidos los pdf que son las guias de los trabajos practicos de la materia. Por el momento solamente se encuentran disponibles las primeras 5 de las 7 practicas. La idea es ir incorporando las ultimas dos practicas y sus respectivas resoluciones mas adelante.
